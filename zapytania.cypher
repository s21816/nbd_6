ZAD 1
MATCH p = ((Darjeeling:town {name:"Darjeeling"})-[r:trek*]-(Sandakphu:peak {name:"Sandakphu"})) UNWIND RELATIONSHIPS(p) AS rel 
WITH p, COUNT(DISTINCT rel.name) AS nRoutes 
RETURN p, nRoutes 
ORDER BY nRoutes 
LIMIT 3;

ZADANIE 2
MATCH p=((Darjeeling:town {name:"Darjeeling"})-[r:trek* {winter: "true"}]-(Sandakphu:peak {name:"Sandakphu"})) UNWIND RELATIONSHIPS(p) AS rel 
WITH p, COUNT(DISTINCT rel.name) AS nRoutes 
RETURN p, nRoutes 
ORDER BY nRoutes 
LIMIT 3;

ZADANIE 3
MATCH p=((Darjeeling:town {name:"Darjeeling"})-[r:twowheeler {summer: "true"}]-(place)) 
RETURN place
  // akurat tu nic nie zwraca bo nie ma takiej drogi....

ZADANIE 4
MATCH (n:Airport)-[r:ORIGIN]-(m) RETURN n, COUNT(r) as departures ORDER BY departures DESC LIMIT 10


ZADANIE 5
MATCH (start:Airport {name:'LAX'})
CALL algo.shortestPath.deltaStepping.stream(start, 'weight', 3.0, {
    nodeQuery:'MATCH(n:Airport) RETURN id(n) AS id',
    relationshipQuery:'MATCH (n:Airport)-[:ORIGIN]-(f)-[:DESTINATION]-(m:Airport),(f)-[r:ASSIGN]-(t:Ticket) RETURN id(n) AS source, id(m) AS target, min(t.price) AS weight',
    relationshipWeightProperty: 'weight',
    graph:'cypher',
    defaultValue:1.0, write:true, writeProperty:'sssp'})
YIELD nodeId, distance
WHERE distance < 3000
RETURN algo.asNode(nodeId).name  AS destination, distance AS cost



ZADANIE 6
MATCH (start:Airport {name:'LAX'}), (end:Airport {name:'DAY'})
CALL algo.kShortestPaths.stream(start,end,4,'price',{
  nodeQuery:'MATCH(n:Airport) RETURN id(n) AS id',
  relationshipQuery:'MATCH (n:Airport)-[:ORIGIN]-(f)-[:DESTINATION]-(m:Airport),(f)-[r:ASSIGN]-(t:Ticket) RETURN id(n) AS source, id(m) AS target, min(t.price) AS weight',
  graph:'cypher',
  writePropertyPrefix: 'cypher_'
})
YIELD nodeIds, costs
RETURN [node in algo.getNodesById(nodeIds) | node.name] AS places, costs, reduce(acc = 0.0, cost in costs | acc + cost) AS totalCost
  // akurat tu nic nie zwraca bo nie ma takiej drogi....

ZADANIE 7
MATCH (start:Airport {name:'LAX'}), (end:Airport {name:'DAY'})
CALL algo.shortestPath.stream(start,end,'price',{
  nodeQuery:'MATCH(n:Airport) RETURN id(n) AS id',
  relationshipQuery:'MATCH (n:Airport)-[:ORIGIN]-(f)-[:DESTINATION]-(m:Airport),(f)-[r:ASSIGN]-(t:Ticket) RETURN id(n) AS source, id(m) AS target, min(t.price) AS weight',
  graph:'cypher',
  writePropertyPrefix: 'cypher_'
})
YIELD nodeId, cost
RETURN algo.getNodeById(nodeId).name, cost
  // akurat tu nic nie zwraca bo nie ma takiej drogi....


ZADANIE 8
MATCH (start:Airport {name:'LAX'}), (end:Airport {name:'DAY'})
CALL algo.shortestPath.stream(start,end,'price',{
  nodeQuery:'MATCH(n:Airport) RETURN id(n) AS id',
  relationshipQuery:'MATCH (n:Airport)-[:ORIGIN]-(f)-[:DESTINATION]-(m:Airport),(f)-[r:ASSIGN]-(t:Ticket {class:"business"}) RETURN id(n) AS source, id(m) AS target, t.price AS weight',
  graph:'cypher',
  writePropertyPrefix: 'cypher_'
})
YIELD nodeId, cost
RETURN algo.getNodeById(nodeId).name, cost
  // akurat tu nic nie zwraca bo nie ma takiej drogi....

ZADANIE 9
MATCH (n:Airport)-[:ORIGIN]-(f)-[:DESTINATION]-(m:Airport) WITH DISTINCT n.name as start, m.name as stop, f.airline as fly RETURN fly as airline, COUNT(*) as number_of_destinations

